package health;

public class BMICalculator {

	String metricsType;
	double height;
	double weight;
	double BMI;
	String underWeight = "This means you're underweight, please feed well";
	String normalWeight = "This means you're OK, continue the healthy diet and the sport you do";
	String overWeight = "This means you're overweight, do some sports and start a healthy diet";
	String obese = "This means you're obese, probably you should contact a nutritionist or a doctor";
	
	public BMICalculator(String metricsType, double height, double weight) {
		this.metricsType = metricsType;
		if(metricsType.equals("metric")) {
			if(height<1.0 || height>2.3) {
				throw new IllegalArgumentException("The height is not correct, please give it in meters between 1.0m and 2.3m");
			}
			else {
				this.height = height;			
			}
		}
		else if(metricsType.equals("US")) {
			if(height<39.0 || height>90.55) {
				throw new IllegalArgumentException("The height is not correct, please give it in inches between 39.0in and 90.55in");				
			}
			else {
			this.height = height*0.0254;
			}
		}
		else {
			throw new IllegalArgumentException("Sorry, cannot recognize this unit, please write \"metric\" or \"US\"");		
		}
		if(metricsType.equals("metric")) {
			if(weight<30.0 || weight>200.0) {
				throw new IllegalArgumentException("The weight is not correct, please give it in kilograms between 30kg and 200kg");
			}
			else {
				this.weight = weight;
			}
		}
		else if(metricsType.equals("US")) {
			if(weight<66.14 || weight>440.92) {
				throw new IllegalArgumentException("The weight is not correct, please give it in pounds between 66.14lbs and 440.92lbs");				
			}
			else {
			this.weight = weight*0.4536;
			}
		}
		else {
			throw new IllegalArgumentException("Sorry, cannot recognize this unit, please give it in kilograms or pounds");		
		}
	}

	public String getMetricsType() {
		return metricsType;
	}
	public double getHeight() {
		return height;
	}
		
	public double getWeight() {
		return weight;
	}

	public double calculateBMI(double height, double weight) {
		BMI = weight/(height*height);
		return BMI;
	}
}
